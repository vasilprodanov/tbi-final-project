﻿using EmailAppManager.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBI_Project.Data.Context;
using TBI_Project.Data.Models;

namespace EmailAppManager.Services.Services
{
    public class UserService : IUserService
    {
        private readonly EmailAppManagerContext context;

        public UserService(EmailAppManagerContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<IReadOnlyCollection<AppUser>> SearchAsync(string criteria)
        {
            return await this.context.AppUsers
                   .Where(p => p.UserName.Contains(criteria) || p.Email.Contains(criteria))
                   .ToListAsync();
        }
    }
}
