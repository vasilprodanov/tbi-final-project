﻿using EmailAppManager.Services.Contracts;
using System.Net.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TBI_Project.Data.Context;
using TBI_Project.Data.GoogleConfig;
using TBI_Project.Data.Models;

namespace EmailManager.Services
{
    public class GoogleAuthService : IGoogleAuthService
    {
        private const int EXPIRATION_MINUTES = 5;

        private readonly IHttpClientFactory factory;
        private readonly GoogleAuthOptions options;
        private readonly EmailAppManagerContext context;

        public GoogleAuthService(IHttpClientFactory factory,
            IOptions<GoogleAuthOptions> googleAuthOptions,
            EmailAppManagerContext context)
        {
            this.factory = factory ?? throw new ArgumentNullException(nameof(factory));
            this.options = googleAuthOptions?.Value ?? throw new ArgumentNullException(nameof(googleAuthOptions));
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<string> GetAccessToken(GoogleCredentials entityCredentials)
        {
            if (DateTime.Now >= entityCredentials.ExpirationTime.AddMinutes(-EXPIRATION_MINUTES))
            {
                var accessToken = await RefreshAccessTokenAsync(entityCredentials.RefreshToken);
                return accessToken;
            }
            else
            {
                return entityCredentials.AccessToken;
            }
        }

        #region RefreshToken
        public async Task<string> RefreshAccessTokenAsync(string refreshToken)
        {
            var response = await this.factory
                       .CreateClient(refreshToken)
                       .PostAsync(this.options.TokenUrl, CreateRefreshTokenRequestBody(refreshToken));
            if (response.IsSuccessStatusCode)
            {
                var deserializedData = JsonConvert.DeserializeObject<GoogleAccessToken>(await response.Content.ReadAsStringAsync());

                await UpdateCredentials(deserializedData);

                return deserializedData.AccessToken;
            }
            else
            {
                //return Json(await response.Content.ReadAsStringAsync());
                throw new ArgumentException("Bad response 2!");
            }
        }

        public FormUrlEncodedContent CreateRefreshTokenRequestBody(string refreshToken)
            => new FormUrlEncodedContent(new[]
            {
                        new KeyValuePair<string, string>("client_id", this.options.ClientId),
                        new KeyValuePair<string, string>("client_secret", this.options.ClientSecret),
                        new KeyValuePair<string, string>("refresh_token", refreshToken),
                        new KeyValuePair<string, string>("grant_type", "refresh_token"),
            });

        #endregion
        #region AccessToken
        #region AcquireCredentials
        public async Task<string> AcquireCredentialsAsync(string authorizationCode)
        {
            var response = await this.factory
                .CreateClient()
                .PostAsync(this.options.TokenUrl, CreateAccessTokenRequestBody(authorizationCode));
            if (response.IsSuccessStatusCode)
            {
                var deserializedData = JsonConvert.DeserializeObject<GoogleAccessToken>(await response.Content.ReadAsStringAsync());

                SaveCredentials(deserializedData);

                return deserializedData.AccessToken;
            }
            else
            {
                //return Json(await response.Content.ReadAsStringAsync());
                throw new ArgumentException("Bad response 1!");
            }
        }
        private FormUrlEncodedContent CreateAccessTokenRequestBody(string authorizationCode)
            => new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("code", authorizationCode),
                new KeyValuePair<string, string>("client_id", this.options.ClientId),
                new KeyValuePair<string, string>("client_secret", this.options.ClientSecret),
                new KeyValuePair<string, string>("redirect_uri", this.options.RedirectUrl),
                new KeyValuePair<string, string>("grant_type", "authorization_code"),
            });
        #endregion
        #endregion

        public void SaveCredentials(GoogleAccessToken tokens)
        {
            this.context.GoogleCredentials.Add(new GoogleCredentials
            {
                AccessToken = tokens.AccessToken,
                RefreshToken = tokens.RefreshToken,
                ExpirationTime = DateTime.Now.AddSeconds(tokens.ExpireIn),
            });
            this.context.SaveChanges();
        }

        public string BuildConsentUrl()
            => new StringBuilder()
            .Append($"{this.options.ConsentUrl}?")
            .Append($"scope= {string.Join(",", this.options.Scopes)}")
            .Append($"&access_type=offline")
            .Append("&include_granted_scopes=true")
            .Append("&response_type=code")
            .Append($"&redirect_uri={this.options.RedirectUrl}")
            .Append($"&client_id={this.options.ClientId}")
            .ToString();

        public async Task<GoogleCredentials> CheckForTokensAsync()
        {
            var credentials = await this.context.GoogleCredentials
                .FirstOrDefaultAsync();
            return credentials;
        }

        public async Task<GoogleCredentials> UpdateCredentials(GoogleAccessToken tokens)
        {
            var credentials = await this.context.GoogleCredentials
                .FirstOrDefaultAsync();
            credentials.AccessToken = tokens.AccessToken;
            credentials.ExpirationTime = DateTime.Now.AddSeconds(tokens.ExpireIn);

            await context.SaveChangesAsync();
            return credentials;
        }
    }
}
