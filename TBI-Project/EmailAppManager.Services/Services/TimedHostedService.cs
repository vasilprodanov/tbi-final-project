﻿using EmailAppManager.Services.Contracts;
using EmailManager.Services.Contracts;
using Google.Apis.Gmail.v1;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EmailAppManager.Services
{
    public class TimedHostedService : IHostedService, IDisposable
    {
        private Timer timer;
        private readonly IServiceProvider services;

        public TimedHostedService(IServiceProvider serviceProvider)
        {
            this.services = serviceProvider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(60));

            return Task.CompletedTask;
        }

        private async void DoWork(object state)
        {
            using (var scope = this.services.CreateScope())
            {
                var gmailAuthService = scope.ServiceProvider.GetRequiredService<IGoogleAuthService>();

                var credentials = await gmailAuthService.CheckForTokensAsync();

                if (credentials == null)
                {
                    return;
                }
                else
                {
                    var accessToken = await gmailAuthService.GetAccessToken(credentials);

                    var gmailApiService = scope.ServiceProvider.GetRequiredService<IGmailApiService>();

                    await gmailApiService.SyncEmails(accessToken);

                    //this.logger.LogInformation("Scoped service id: " + service.Id);
                }
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            timer?.Dispose();
        }
    }
}
