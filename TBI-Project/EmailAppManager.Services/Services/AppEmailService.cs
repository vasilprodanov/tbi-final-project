﻿using EmailAppManager.Services.Contracts;
using EmailManager.Services.Contracts;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TBI_Project.Data.Context;
using TBI_Project.Data.GoogleConfig;
using TBI_Project.Data.Models;

namespace EmailAppManager.Services
{
    public class AppEmailService : IAppEmailService
    {
        private readonly EmailAppManagerContext context;
        private readonly IGmailApiService apiService;
        private readonly IGoogleAuthService googleService;

        public AppEmailService(EmailAppManagerContext context,
               IGmailApiService apiService,
               IGoogleAuthService googleService)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.apiService = apiService ?? throw new ArgumentNullException(nameof(apiService));
            this.googleService = googleService ?? throw new ArgumentNullException(nameof(googleService));
        }

        public List<Message> ListMessages(GmailService service, string userId)
        {
            List<Message> result = new List<Message>();
            UsersResource.MessagesResource.ListRequest request = service.Users.Messages.List(userId);


            ListMessagesResponse response = request.Execute();
            result.AddRange(response.Messages);
            request.PageToken = response.NextPageToken;

            //var firstMessage = service.Users.Messages.Get(userId, result.First().Id).Execute();
            var payload = new StringBuilder();
            var emailsWithComponents = new List<Email>();

            foreach (var message in result)
            {
                var email = new Email();
                email.OriginalId = message.Id;

                foreach (var part in message.Payload.Parts)
                {
                    byte[] data = FromBase64ForUrlString(part.Body.Data);
                    string decodedString = Encoding.UTF8.GetString(data);
                    payload.Append(decodedString);
                }
            }
            var mailMessage = payload.ToString();


            return result;
        }

        public IReadOnlyCollection<Email> GetAllEmails(int take = int.MaxValue)
        {
            return this.context.Emails.Where(e => !e.IsDeleted)
                .OrderByDescending(e => e.StoredOn)
                .ThenByDescending(e => e.ID).Take(take).ToList();
        }

        public IReadOnlyCollection<Email> GetLastModifiedEmails(int take = int.MaxValue)
        {
            return this.context.Emails.Where(e => !e.IsDeleted)
                .OrderByDescending(e => e.LastModifiedOn)
                .ThenByDescending(e => e.ID).Take(take).ToList();
        }

        public IReadOnlyCollection<Email> GetAllEmailsByStatus(string statusName, int take = int.MaxValue)
        {
            return this.context.Emails.Where(e => !e.IsDeleted && e.Status.StatusName == statusName)
                .OrderByDescending(e => e.StoredOn)
                .ThenByDescending(e => e.ID).Take(take).ToList();
        }

        public IReadOnlyCollection<Email> GetApprovedAndNewEmails(int take = int.MaxValue)
        {
            return this.context.Emails
                .Where(e => !e.IsDeleted
                 && (e.Status.StatusName.ToLower() == "approved" || e.Status.StatusName.ToLower() == "new"))
                .OrderByDescending(e => e.LastModifiedOn)
                .Take(take)
                .ToList();
        }

        //public IReadOnlyCollection<Email> SyncEmails(GmailService service, string userId)
        //{

        //    var inboxCount = (int)service.Users.GetProfile(userId).Execute().MessagesTotal;
        //    var dbCount = this.context.Emails.Count();

        //    if (inboxCount > dbCount)
        //    {
        //        var unsyncEmails = inboxCount - dbCount;
        //        service.Users.Messages.List(userId).MaxResults = unsyncEmails;
        //        var emailsToSync = service.Users.Messages.List(userId).Execute().Messages.Skip(dbCount);
        //        //var result = new List<Message>();
        //        // result.AddRange(emailsToSync.Messages);

        //        var newEmails = new List<Email>();
        //        var initialStatus = context.EmailStatuses
        //                            .FirstOrDefault(x => x.StatusName.ToLower() == "not reviewed");
        //        foreach (var email in emailsToSync)
        //        {
        //            var emailToAdd = new Email
        //            {
        //                OriginalId = email.Id,
        //                StoredOn = DateTime.Now,
        //                Status = initialStatus,
        //                StatusID = initialStatus.ID,
        //            };

        //            //DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        //            //emailToAdd.ReceivedOn = start.AddMilliseconds((double)email.InternalDate).ToLocalTime();

        //            this.context.Emails.Add(emailToAdd);
        //            newEmails.Add(emailToAdd);
        //        }
        //        context.SaveChanges();
        //        return newEmails;
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        public async Task<Email> RevertStatusAsync(int id, string userId)
        {
            var email = await this.context.Emails.FindAsync(id);
            var user = await this.context.AppUsers.FindAsync(userId);
            var status = await this.context.EmailStatuses.FindAsync(email.StatusID);
            var currentStatus = status.StatusName.ToLower();

            if (currentStatus == "new" || currentStatus == "not valid")
            {
                var newStatus = this.context.EmailStatuses.FirstOrDefault(es => es.StatusName.ToLower() == "not reviewed");
                email.Status = newStatus;
                email.StatusID = newStatus.ID;
                email.LastModifiedOn = DateTime.Now;
                email.ProcessedBy = user;
                email.ProcessedById = user.Id;
            }
            else if (currentStatus == "rejected" || currentStatus == "approved")
            {
                var newStatus = this.context.EmailStatuses.FirstOrDefault(es => es.StatusName.ToLower() == "open");
                email.Status = newStatus;
                email.StatusID = newStatus.ID;
                email.LastModifiedOn = DateTime.Now;
                email.ProcessedBy = user;
                email.ProcessedById = user.Id;
            }

            await this.context.SaveChangesAsync();

            return email;
        }

        public async Task<Email> HandleRejectStatusesAsync(int id, string userId)
        {
            var email = await this.context.Emails.FindAsync(id);
            var user = await this.context.AppUsers.FindAsync(userId);
            var status = await this.context.EmailStatuses.FindAsync(email.StatusID);
            var currentStatus = status.StatusName.ToLower();

            if (currentStatus == "not reviewed")
            {
                var newStatus = this.context.EmailStatuses.FirstOrDefault(es => es.StatusName.ToLower() == "not valid");
                email.PrevStatusName = currentStatus;
                email.Status = newStatus;
                email.StatusID = newStatus.ID;
                email.LastModifiedOn = DateTime.Now;
                email.ProcessedBy = user;
                email.ProcessedById = user.Id;
            }
            else if (currentStatus == "open")
            {
                var newStatus = this.context.EmailStatuses.FirstOrDefault(es => es.StatusName.ToLower() == "rejected");
                email.Status = newStatus;
                email.StatusID = newStatus.ID;
                email.LastModifiedOn = DateTime.Now;
                email.ProcessedBy = user;
                email.ProcessedById = user.Id;
            }

            await context.SaveChangesAsync();
            return email;
        }

        public async Task<Email> HandleSuccessStatusesAsync(int emailId, string userId)
        {
            var email = await this.context.Emails.FindAsync(emailId);
            var user = await this.context.AppUsers.FindAsync(userId);
            var status = await this.context.EmailStatuses.FindAsync(email.StatusID);
            var currentStatus = status.StatusName.ToLower();

            if (currentStatus == "not reviewed")
            {
                var credentials = await googleService.CheckForTokensAsync();
                var token = await this.googleService.GetAccessToken(credentials);
                email = await this.apiService.GetMessageBodyAsync(email.ID, token);
                email.PrevStatusName = email.Status.StatusName;
                var newStatus = this.context.EmailStatuses.FirstOrDefault(es => es.StatusName.ToLower() == "new");
                email.PrevStatusName = currentStatus;
                email.Status = newStatus;
                email.StatusID = newStatus.ID;
                email.LastModifiedOn = DateTime.Now;
                email.ProcessedBy = user;
                email.ProcessedById = user.Id;
            }
            else if (currentStatus == "open")
            {
                var newStatus = this.context.EmailStatuses.FirstOrDefault(es => es.StatusName.ToLower() == "approved");
                email.Status = newStatus;
                email.StatusID = newStatus.ID;
                email.LastModifiedOn = DateTime.Now;
                email.ProcessedBy = user;
                email.ProcessedById = user.Id;
            }
            await context.SaveChangesAsync();
            return email;
        }

        public byte[] FromBase64ForUrlString(string base64ForUrlInput)
        {
            int padChars = (base64ForUrlInput.Length % 4) == 0 ? 0 : (4 - (base64ForUrlInput.Length % 4));
            StringBuilder result = new StringBuilder(base64ForUrlInput, base64ForUrlInput.Length + padChars);
            result.Append(string.Empty.PadRight(padChars, '='));
            result.Replace('-', '+');
            result.Replace('_', '/');

            return Convert.FromBase64String(result.ToString());
        }

        public (int, int) GetAttachmentInformation(Message msg)
        {
            var count = 0;
            var size = 0;
            foreach (var part in msg.Payload.Parts)
            {
                if (part.Body.AttachmentId != null)
                {
                    count += 1;
                    size += (int)part.Body.Size;
                }
            }

            return (count, size);
        }



        public Email MarkEmailAsNew(int id)
        {
            var email = this.context.Emails.FirstOrDefault(e => e.ID == id);

            var status = this.context.EmailStatuses.FirstOrDefault(s => s.StatusName == "new");

            //var statusID = this.context.EmailStatuses.FirstOrDefault(s => s.ID == 2);

            email.Status = status;
            email.StatusID = status.ID;

            context.SaveChanges();

            return email;
        }

        public Email MarkEmailAsOpen(int id)
        {
            var email = this.context.Emails.FirstOrDefault(e => e.ID == id);

            var status = this.context.EmailStatuses.FirstOrDefault(s => s.StatusName == "open");

            email.Status = status;

            return email;
        }

        public Email MarkEmailAsApproved(int id)
        {
            var email = this.context.Emails.FirstOrDefault(e => e.ID == id);

            var status = this.context.EmailStatuses.FirstOrDefault(s => s.StatusName == "approved");

            email.Status = status;

            return email;
        }

        public Email MarkEmailAsRejected(int id)
        {
            var email = this.context.Emails.FirstOrDefault(e => e.ID == id);

            var status = this.context.EmailStatuses.FirstOrDefault(s => s.StatusName == "rejected");

            email.Status = status;

            return email;
        }

        public Email GetEmail(int id)
          => this.context.Emails
                 .FirstOrDefault(p => p.ID == id);


        public async void SetInitialStatusAsync()
        {
            var initialStatus = await this.context.EmailStatuses
                .FirstOrDefaultAsync(s => s.StatusName.ToLower() == "not reviewed");

            var updated = this.context.Emails.Where(e => e.Status == null).ToList()
                .Select(e => { e.Status = initialStatus; e.StatusID = initialStatus.ID; return e; });

            await this.context.SaveChangesAsync();
        }

        
        public async Task<IList<Email>> ListAllEmailsAsync(int skip, int pageSize, string searchValue)
        {
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.ToLower();

                var emails = await this.context.Emails
                     .Where(a => a.Subject.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.ID)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();

                return emails;
            }
            else
            {
                var emails = await this.context.Emails
                    .OrderByDescending(a => a.ID)
                    .Skip(skip)
                    .Take(pageSize)
                    .ToListAsync();

                return emails;
            }
        }

        public int GetEmailsCount()
        {
            return this.context.Emails.Count();
        }
    }
}
