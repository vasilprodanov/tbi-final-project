﻿using Crypteron.CipherObject;
using EmailAppManager.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBI_Project.Data.Context;
using TBI_Project.Data.Models;

namespace EmailAppManager.Services.Services
{
    public class ApplicationServices : IApplicationServices
    {
        private readonly EmailAppManagerContext context;

        public ApplicationServices(EmailAppManagerContext context)
        {
            this.context = context;
        }

        public Application CreateApplication(IReadOnlyList<string> applicationParameters, string customerID, decimal amount, int period)
        {

            var applicationToCreate = new Application();

            applicationToCreate.CustomerID = customerID;
            applicationToCreate.CustomerFirstName = applicationParameters[0];
            applicationToCreate.CustomerSecondName = applicationParameters[1];
            applicationToCreate.CustomerFullName = $"{applicationToCreate.CustomerFirstName} {applicationToCreate.CustomerSecondName}";
            applicationToCreate.PhoneNumber = applicationParameters[2];
            applicationToCreate.RegisteredOn = DateTime.Now;
            applicationToCreate.LastChangedOn = DateTime.Now;
            applicationToCreate.ApplicationStatusID = 1;
            applicationToCreate.LoanAmount = amount;
            applicationToCreate.LoanPeriodInMonths = period;
            applicationToCreate.EmailOriginalID = applicationParameters[3];

            //applicationToCreate.Seal();

            context.Applications.Add(applicationToCreate);
            //var email = this.context.Emails.FirstOrDefault(e => e.OriginalId == applicationToCreate.EmailOriginalID);
            //email.IsDeleted = true;

            context.SaveChanges();

            return applicationToCreate;
        }

        public async Task<Application> GetApplication(int id)
        => await this.context.Applications
                     .FindAsync(id);

        public async Task<Application> HandleRejectStatusesAsync(int id, string userId)
        {
            var application = await this.context.Applications.FindAsync(id);
            var user = await this.context.AppUsers.FindAsync(userId);
            var status = await this.context.ApplicationStatuses.FindAsync(application.ApplicationStatusID);
            var currentStatus = status.StatusName.ToLower();

            //if (currentStatus == "not reviewed")
            //{
            //    var newStatus = this.context.EmailStatuses.FirstOrDefault(es => es.StatusName.ToLower() == "not valid");
            //    email.Status = newStatus;
            //    email.StatusID = newStatus.ID;
            //    email.LastModifiedOn = DateTime.Now;
            //    email.ProcessedBy = user;
            //    email.ProcessedById = user.Id;
            //}
            if (currentStatus == "open")
            {
                var newStatus = this.context.ApplicationStatuses.FirstOrDefault(es => es.StatusName.ToLower() == "rejected");
                application.Status = newStatus;
                application.ApplicationStatusID = newStatus.ID;
                application.LastChangedOn = DateTime.Now;
                application.ProcessedBy = user;
                application.ProcessedById = user.Id;
            }
            await context.SaveChangesAsync();
            return application;
        }

        public async Task<Application> HandleSuccessStatusesAsync(int Id, string userId)
        {
            var application = await this.context.Applications.FindAsync(Id);
            var user = await this.context.AppUsers.FindAsync(userId);
            var status = await this.context.ApplicationStatuses.FindAsync(application.ApplicationStatusID);
            var currentStatus = status.StatusName.ToLower();

            //if (currentStatus == "not reviewed")
            //{
            //    var credentials = await googleService.CheckForTokensAsync();
            //    var token = await this.googleService.GetAccessToken(credentials);
            //    email = await this.apiService.FetchDetailsAsync(email.ID, token);
            //    email.PrevStatusName = email.Status.StatusName;
            //    var newStatus = this.context.EmailStatuses.FirstOrDefault(es => es.StatusName.ToLower() == "new");
            //    email.Status = newStatus;
            //    email.StatusID = newStatus.ID;
            //    email.LastModifiedOn = DateTime.Now;
            //    email.ProcessedBy = user;
            //    email.ProcessedById = user.Id;
            //}
            if (currentStatus == "open")
            {
                var newStatus = this.context.ApplicationStatuses.FirstOrDefault(es => es.StatusName.ToLower() == "approved");
                application.Status = newStatus;
                application.ApplicationStatusID = newStatus.ID;
                application.LastChangedOn = DateTime.Now;
                application.ProcessedBy = user;
                application.ProcessedById = user.Id;
            }
            await context.SaveChangesAsync();
            return application;
        }

        public async Task<Application> RevertStatusAsync(int id, string userId)
        {
            var application = await this.context.Applications.FindAsync(id);
            var user = await this.context.AppUsers.FindAsync(userId);
            var status = await this.context.ApplicationStatuses.FindAsync(application.ApplicationStatusID);
            var currentStatus = status.StatusName.ToLower();

            //if (currentStatus == "new" || currentStatus == "not valid")
            //{
            //    var newStatus = this.context.EmailStatuses.FirstOrDefault(es => es.StatusName.ToLower() == "not reviewed");
            //    email.Status = newStatus;
            //    email.StatusID = newStatus.ID;
            //    email.LastModifiedOn = DateTime.Now;
            //    email.ProcessedBy = user;
            //    email.ProcessedById = user.Id;
            //} 

            //This should be edited, from close to New, not open
            if (currentStatus == "rejected" || currentStatus == "approved")
            {
                var newStatus = this.context.ApplicationStatuses.FirstOrDefault(es => es.StatusName.ToLower() == "open");
                application.Status = newStatus;
                application.ApplicationStatusID = newStatus.ID;
                application.LastChangedOn = DateTime.Now;
                application.ProcessedBy = user;
                application.ProcessedById = user.Id;
            }

            else if (currentStatus == "open")
            {
                application.IsDeleted = true;
            }

            await this.context.SaveChangesAsync();

            return application;
        }

        //public Application UnsealApplication(Application app)
        //=> app.Unseal();

        public int GetApplicationsCount()
        {
            return this.context.Applications.Count();
        }

        public int GetClosedApplicationsCount()
        {
            var count = this.context.Applications
               .Where(a => a.ApplicationStatusID == 2
               || a.ApplicationStatusID == 3)
               .Count();

            return count;
        }

        public int GetOpenApplicationsCount()
        {
            var count = this.context.Applications
                .Where(a => a.ApplicationStatusID == 1)
                .Count();

            return count;
        }
    }
}
