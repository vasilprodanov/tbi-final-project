﻿using EmailAppManager.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBI_Project.Data.Context;
using TBI_Project.Data.Models;

namespace EmailAppManager.Services.Services
{
    public class ListingServices : IListingServices
    {
        private readonly EmailAppManagerContext context;
        private readonly IApplicationServices appService;

        public ListingServices(
            EmailAppManagerContext context,
            IApplicationServices appService)
        {
            this.context = context;
            this.appService = appService;
        }

        public IReadOnlyCollection<Email> GetAllEmails()
           => this.context.Emails.Where(e => !e.IsDeleted).OrderByDescending(e => e.ID).ToList();

        public async Task<IReadOnlyCollection<Email>> GetAllValidEmailsAsync(int skip, int pageSize, string searchValue)
        {
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.ToLower();

                var emails = await this.context.Emails
                     .Where(e => !e.IsDeleted && e.Status.ID == 2)
                     .Where(a => a.Subject.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.ID)
                     .Skip(skip)
                     .Take(pageSize)
                     .ToListAsync();

                return emails;
            }
            else
            {
                var emails = await this.context.Emails
                    .Where(e => !e.IsDeleted && e.Status.ID == 2)
                    .OrderByDescending(a => a.ID)
                    .Skip(skip)
                    .Take(pageSize)
                    .ToListAsync();

                return emails;
            }
        }
        public async Task<IReadOnlyCollection<Application>> GetAllOpenApplicationsAsync(int skip, int pageSize, string searchValue)
        {
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.ToLower();

                var applications = await this.context.Applications
                     .Where(a => !a.IsDeleted && a.ApplicationStatusID == 1)
                     .Where(a => a.CustomerFirstName.ToLower()
                     .StartsWith(searchValue)
                     || a.CustomerSecondName.ToLower()
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.ID)
                     .Skip(skip)
                     .Take(pageSize)
                     //.Select(a => this.appService.UnsealApplication(a))
                     .ToListAsync();

                return applications;
            }
            else
            {
                var applications = await this.context.Applications
                    .Where(a => !a.IsDeleted && a.ApplicationStatusID == 1)
                    .OrderByDescending(a => a.ID)
                    .Skip(skip)
                    .Take(pageSize)
                    //.Select(a => this.appService.UnsealApplication(a))
                    .ToListAsync();

                return applications;
            }
        }

        public async Task<IReadOnlyCollection<Application>> GetAllClosedApplicationsAsync(int skip, int pageSize, string searchValue)
        {
            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.ToLower();

                var applications = await this.context.Applications
                     .Where(a => !a.IsDeleted && (a.ApplicationStatusID == 2
                     || a.ApplicationStatusID == 3))
                     .Where(a => a.CustomerFirstName.ToLower()
                     .StartsWith(searchValue)
                     || a.CustomerSecondName.ToLower()
                     .StartsWith(searchValue)
                     || a.CustomerID
                     .StartsWith(searchValue))
                     .OrderByDescending(a => a.ID)
                     .Skip(skip)
                     .Take(pageSize)
                     //.Select(a => this.appService.UnsealApplication(a))
                     .ToListAsync();

                return applications;
            }
            else
            {
                var applications = await this.context.Applications
                     .Where(a => !a.IsDeleted && (a.ApplicationStatusID == 2
                     || a.ApplicationStatusID == 3))
                     .OrderByDescending(a => a.ID)
                     .Skip(skip)
                     .Take(pageSize)
                     //.Select(a => this.appService.UnsealApplication(a))
                     .ToListAsync();

                return applications;
            }
        }
        public int GetValidEmailsCount()
        {
            return this.context.Emails
                .Where(e => e.Status.StatusName.ToLower() == "new" || e.Status.StatusName.ToLower() == "approved")
                .Count();
        }
    }
}
