﻿using Crypteron.CipherObject;
using EmailAppManager.Services.Contracts;
using EmailManager.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TBI_Project.Data.Context;
using TBI_Project.Data.GoogleConfig;
using TBI_Project.Data.Models;

namespace EmailManager.Services
{
    public class GmailApiService : IGmailApiService
    {
        private readonly IHttpClientFactory factory;
        private readonly GmailApiUrlOptions gmailUrlOptions;
        private readonly IGoogleAuthService gmailAuthService;
        private readonly EmailAppManagerContext context;

        public GmailApiService(IHttpClientFactory factory,
            IOptions<GmailApiUrlOptions> gmailOptions,
            IGoogleAuthService gmailAuthService,
            EmailAppManagerContext context)
        {
            this.factory = factory ?? throw new ArgumentNullException(nameof(factory));
            this.gmailUrlOptions = gmailOptions?.Value ?? throw new ArgumentNullException(nameof(gmailOptions));
            this.gmailAuthService = gmailAuthService ?? throw new ArgumentNullException(nameof(gmailAuthService));
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public async Task<IReadOnlyCollection<Email>> SyncEmails(string accessToken)
        {
            var newMessagesList = new List<GmailMessageObject>();
            var messageList = await this.SendRequestAsync<GmailMessageListObject>(this.gmailUrlOptions.Messages, accessToken);

            var messages = messageList.Messages;

            //var userProfile = this.SendRequestAsync<GmailUserProfileObject>(this.gmailUrlOptions.UserProfile, accessToken);
            var lastAddedMessageMailboxId = (await this.context.Emails.FirstOrDefaultAsync())?.OriginalId;

            var newEmails = new List<Email>();
            var initialStatus = context.EmailStatuses
                                .FirstOrDefault(x => x.StatusName.ToLower() == "not reviewed");

            foreach (var message in messageList.Messages)
            {

                var messageWithDetails = await this.SendRequestAsync<GmailMessageObject>(this.gmailUrlOptions.Message + message.Id, accessToken);
                var emailToAdd = new Email
                {
                    OriginalId = message.Id,
                    StoredOn = DateTime.Now,
                    Status = initialStatus,
                    StatusID = initialStatus.ID,
                };

                newEmails.Add(emailToAdd);

                await this.ParseDetailsWithoutBody(message, emailToAdd, accessToken);
                await this.ModifyLabelsAsync(this.gmailUrlOptions.Message + message.Id, accessToken);
            }

            context.SaveChanges();

            return newEmails;
        }

        public async Task<Email> FetchMessageBodyAsync(int emailId, string accessToken)
        {
            var email = this.context.Emails.Find(emailId);

            if (!string.IsNullOrEmpty(email.Body))
            {
                return email;
            }

            var mailboxEmailId = email.OriginalId;

            var message = await this.SendRequestAsync<GmailMessageObject>(this.gmailUrlOptions.Message + mailboxEmailId, accessToken);

            string body = "";

            IList<MessagePartHeader> parts = message.Payload?.Headers;

            if (parts == null)
            {
                return email;
            }
            else
            {
                foreach (var part in parts)
                {
                    if (message.Payload.Parts == null && message.Payload.Body != null)
                    {
                        body = DecodeBase64String(message.Payload.Body.Data);
                    }
                    else
                    {
                        body = GetNestedBodyParts(message.Payload.Parts, "");
                    }
                }
                if (string.IsNullOrEmpty(body))
                {
                    body = "No content sent!";
                }
                email.Body = body;

                //email.Seal();
                //await this.context.SaveChangesAsync();
            }

            return email;
        }

        public async Task<Email> GetMessageBodyAsync(int emailId, string accessToken)
        {
            var email = await this.context.Emails.FindAsync(emailId);

            if (email.Body != "")
            {
                return email;
            }

            var mailboxEmailId = email.OriginalId;
            var message = await this.SendRequestAsync<GmailMessageObject>(this.gmailUrlOptions.Message + mailboxEmailId, accessToken);

            string body = "";

            IList<MessagePartHeader> parts = message.Payload?.Headers;

            foreach (var part in parts)
            {
                if (message.Payload.Parts == null && message.Payload.Body != null)
                {
                    body = DecodeBase64String(message.Payload.Body.Data);
                }
                else
                {
                    body = GetNestedBodyParts(message.Payload.Parts, "");
                }
            }
            if (string.IsNullOrEmpty(body))
            {
                body = "No content sent!";
            }

            email.Body = body;

            //email.Seal();
            await this.context.SaveChangesAsync();

            return email;
        }
        public async Task<Email> ParseDetailsWithoutBody(GmailMessageObject message, Email email, string accessToken)
        {
            var mailboxEmailId = email.OriginalId;
            message = await this.SendRequestAsync<GmailMessageObject>(this.gmailUrlOptions.Message + mailboxEmailId, accessToken);

            string from = "";
            string dateHeader = "";
            string subject = "";
            string body = "";
            string senderName = "";
            string senderEmail = "";

            IList<MessagePartHeader> parts = message.Payload?.Headers;

            if (parts == null)
            {
                return email;
            }

            else
            {
                foreach (var part in parts)
                {
                    if (part.Name == "From")
                    {
                        from = part.Value;

                        var senderEmailMatch = System.Text.RegularExpressions.Regex.Match(from, "<(.+)>");
                        senderName = from.Substring(0, senderEmailMatch.Index);
                        senderEmail = senderEmailMatch.Groups[1].Value;
                    }
                    else if (part.Name == "Date")
                    {
                        dateHeader = part.Value;
                    }
                    else if (part.Name == "Subject")
                    {
                        subject = part.Value;
                    }
                }

                email.Subject = subject;
                email.Sender = senderEmail;
                email.Body = body;
                email.ReceivedOn = this.ParseDate(dateHeader);
                email.ProcessedBy = null;

               // email.Seal();
                await this.context.SaveChangesAsync();
            }
            return email;
        }

        private async Task<TObject> SendRequestAsync<TObject>(string url, string accessToken)
        {
            var client = this.factory.CreateClient();

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);

            var response = await client.GetAsync(this.gmailUrlOptions.Base + url);

            //Console.WriteLine(res.ToString());
            //Console.WriteLine(await res.Content.ReadAsStringAsync());

            var result = JsonConvert.DeserializeObject<TObject>(await response.Content.ReadAsStringAsync());

            return result;
        }

        private async Task ModifyLabelsAsync(string url, string accessToken)
        {
            var client = this.factory.CreateClient();

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);
            client.DefaultRequestHeaders.Add("Accept", "application/json");

            var modifyMessageLabels = new GmailMessageLabelModifier()
            {
                LabelsToAdd = new List<string> { "SYNCED" },
                LabelsToRemove = new List<string> { "INBOX" }
            };

            var content = JsonConvert.SerializeObject(modifyMessageLabels);

            var httpContent = new StringContent(content, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(this.gmailUrlOptions.Base + url, httpContent);

            await client.PostAsync(this.gmailUrlOptions.Base + url, httpContent);

        }

        private static string DecodeBase64String(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return "<strong>Message body was not returned</strong>";

            string InputStr = input.Replace("-", "+").Replace("_", "/");
            return System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(InputStr));
        }

        private static string GetNestedBodyParts(IList<MessagePart> parts, string curr)
        {
            string str = curr;
            if (parts == null)
            {
                return str;
            }
            else
            {
                foreach (var part in parts)
                {
                    if (part.Parts == null)
                    {
                        if (part.Body != null && part.Body.Data != null)
                        {
                            var ts = DecodeBase64String(part.Body.Data);
                            if (!str.Contains(ts))
                            {
                                str += ts;
                            }
                        }
                    }
                    else
                    {
                        return GetNestedBodyParts(part.Parts, str);
                    }
                }

                return str;
            }
        }

        public DateTime ParseDate(string dateAsString)
        {
            string stringToParse = dateAsString;

            if (dateAsString.Contains('('))
            {
                int index = dateAsString.IndexOf('(') - 1;
                string stringToRemove = dateAsString.Substring(index);
                stringToParse = dateAsString.Replace(stringToRemove, "");
            }

            return DateTime.ParseExact(stringToParse, "ddd, d MMM yyyy HH:m:ss zzzz", CultureInfo.InvariantCulture);
        }
    }
}
