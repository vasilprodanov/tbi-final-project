﻿using EmailAppManager.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TBI_Project.Data.Context;

namespace EmailAppManager.Services.Services
{
    public class RegisterService : IRegisterService
    {
        private readonly EmailAppManagerContext context;

        public RegisterService(EmailAppManagerContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public bool CanRegisterNewUsers()
        {
            bool result = true;

            int registeredUsers = this.context.Users.Count();

            if(registeredUsers == 30)
            {
                result = false;
            }

            return result;
        }
    }
}
