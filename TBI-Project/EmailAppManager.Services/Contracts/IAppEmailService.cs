﻿using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TBI_Project.Data.Models;

namespace EmailAppManager.Services.Contracts
{
    public interface IAppEmailService
    {
        List<Message> ListMessages(GmailService service, string userId);

        //Task<Email> FetchDetailsAsync(GmailService service, string userId, int emailId);

        IReadOnlyCollection<Email> GetAllEmails(int take = int.MaxValue);

        IReadOnlyCollection<Email> GetLastModifiedEmails(int take = int.MaxValue);

        IReadOnlyCollection<Email> GetApprovedAndNewEmails(int take = int.MaxValue);

        IReadOnlyCollection<Email> GetAllEmailsByStatus(string statusName, int take = int.MaxValue);

        Task<Email> RevertStatusAsync(int emailId, string userId);

        Task<Email> HandleRejectStatusesAsync(int emailId, string userId);

        Task<Email> HandleSuccessStatusesAsync(int emailId, string userId);

        Task<IList<Email>> ListAllEmailsAsync(int skip, int pageSize, string searchValue);

        int GetEmailsCount();

        void SetInitialStatusAsync();

        Email MarkEmailAsNew(int Id);

        Email MarkEmailAsOpen(int Id);

        Email MarkEmailAsApproved(int Id);

        Email MarkEmailAsRejected(int Id);

        Email GetEmail(int id);
    }
}
