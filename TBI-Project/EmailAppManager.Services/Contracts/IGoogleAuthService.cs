﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TBI_Project.Data.Models;

namespace EmailAppManager.Services.Contracts
{
    public interface IGoogleAuthService
    {
        Task<string> GetAccessToken(GoogleCredentials entityCredentials);

        Task<GoogleCredentials> CheckForTokensAsync();

        string BuildConsentUrl();

        Task<string> AcquireCredentialsAsync(string authorizationCode);

        Task<string> RefreshAccessTokenAsync(string refreshToken);
    }
}
