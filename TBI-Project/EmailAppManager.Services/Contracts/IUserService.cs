﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TBI_Project.Data.Models;

namespace EmailAppManager.Services.Contracts
{
    public interface IUserService
    {
        Task<IReadOnlyCollection<AppUser>> SearchAsync(string criteria);
    }
}
