﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TBI_Project.Data.Models;

namespace EmailAppManager.Services.Contracts
{
    public interface IListingServices
    {
        IReadOnlyCollection<Email> GetAllEmails();

        Task<IReadOnlyCollection<Email>> GetAllValidEmailsAsync(int skip, int pageSize, string searchValue);

        Task<IReadOnlyCollection<Application>> GetAllOpenApplicationsAsync(int skip, int pageSize, string searchValue);

        Task<IReadOnlyCollection<Application>> GetAllClosedApplicationsAsync(int skip, int pageSize, string searchValue);

        int GetValidEmailsCount();
    }
}
