﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TBI_Project.Data.Models;

namespace EmailAppManager.Services.Contracts
{
    public interface IApplicationServices
    {
        Application CreateApplication(IReadOnlyList<string> applicationParameters, string customerID, decimal amount, int period);

        Task<Application> GetApplication(int id);

        Task<Application> HandleSuccessStatusesAsync(int Id, string userId);

        Task<Application> HandleRejectStatusesAsync(int id, string userId);

        Task<Application> RevertStatusAsync(int id, string userId);
        //Application UnsealApplication(Application app);

        int GetApplicationsCount();

        int GetClosedApplicationsCount();

        int GetOpenApplicationsCount();
    }
}
