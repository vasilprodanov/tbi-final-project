﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmailAppManager.Services.Contracts
{
    public interface IRegisterService
    {
        bool CanRegisterNewUsers();
    }
}
