﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TBI_Project.Data.GoogleConfig;
using TBI_Project.Data.Models;

namespace EmailManager.Services.Contracts
{
    public interface IGmailApiService
    {
        Task<IReadOnlyCollection<Email>> SyncEmails(string accessToken);

        Task<Email> FetchMessageBodyAsync(int emailId, string accessToken);

        Task<Email> GetMessageBodyAsync(int emailId, string accessToken);

        Task<Email> ParseDetailsWithoutBody(GmailMessageObject message, Email email, string accessToken);
    }
}
