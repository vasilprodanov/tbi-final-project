﻿$('.register-header').click(function (ev) {
    ev.preventDefault();

    console.log('click');
    var currentLink = $(ev.target);
    var url = currentLink.data('url');
    var details = currentLink.parent().find('.col-md-12');
    var isClicked = false;

    if (details.length === 0) {
        $.get(url, function (data) {
            currentLink.append(data);
        });

        isClicked = true;
    } else {
        details.remove();
    }
});