﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailAppManager.Models
{
    public class ApplicationDetailsModalModel
    {
        public EmailViewModel Email { get; set; }

        public ApplicationViewModel Application { get; set; }
    }
}
