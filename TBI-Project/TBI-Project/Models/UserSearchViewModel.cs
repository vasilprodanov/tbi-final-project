﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmailAppManager.Models
{
    public class UserSearchViewModel
    {
        public UserSearchViewModel()
        {

        }
        //[Required]
        //[MinLength(3, ErrorMessage = "Please provide at least 3 letters")]
        public string SearchName { get; set; }

        public IReadOnlyList<UserViewModel> SearchResults { get; set; } = new List<UserViewModel>();
    }
}
