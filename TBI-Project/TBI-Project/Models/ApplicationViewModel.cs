﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TBI_Project.Data.Models;

namespace EmailAppManager.Models
{
    public class ApplicationViewModel
    {
        public int ID { get; set; }

        //public string Sender { get; set; }

        //public string Subject { get; set; }

        //public int AttachmentsCount { get; set; }

        //public double AttachmentsSize { get; set; }

        public DateTime RegisteredOn { get; set; }

        public DateTime LastChangedOn { get; set; }

        public DateTime TerminalStatusSetOn { get; set; }

        public string EmailOriginalID { get; set; }

        public bool IsDeleted { get; set; }

        public string CustomerID { get; set; }

        public string CustomerFirstName { get; set; }

        public string CustomerSecondName { get; set; }

        public string CustomerFullName { get; set; }

        public string PhoneNumber { get; set; }

        public decimal LoanAmount { get; set; }

        public int LoanPeriodInMonths { get; set; }

        public int PeriodInMonths { get; set; }

        public string StatusName { get; set; }
        // Navigational properties


        public int ApplicationStatusID { get; set; }
        public ApplicationStatus Status { get; set; }

        public string ProcessedById { get; set; }
        public AppUser ProcessedBy { get; set; }
    }
}
