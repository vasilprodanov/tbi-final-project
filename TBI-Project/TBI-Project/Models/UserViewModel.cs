﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailAppManager.Models
{
    public class UserViewModel
    {
        public UserViewModel() { }

        public string Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Role { get; set; }

        public DateTime RegisteredOn { get; set; }

        public string RegisteredBy { get; set; }
    }
}
