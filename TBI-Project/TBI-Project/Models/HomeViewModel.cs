﻿using EmailAppManager.Services.Contracts;
using Google.Apis.Gmail.v1.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailAppManager.Models
{
    public class HomeViewModel
    {
        public HomeViewModel()
        {
            LatestEmails = new List<EmailViewModel>();
            LastModified = new List<EmailViewModel>();
            ApprovedAndNew = new List<EmailViewModel>();
        }

        public IReadOnlyCollection<EmailViewModel> LatestEmails { get; set; }

        public IReadOnlyCollection<EmailViewModel> LastModified { get; set; }

        public IReadOnlyCollection<EmailViewModel> ApprovedAndNew { get; set; }
    }
}
