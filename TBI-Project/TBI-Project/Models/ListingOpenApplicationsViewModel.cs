﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailAppManager.Models
{
    public class ListingOpenApplicationsViewModel
    {
        public List<ApplicationViewModel> OpenApplications { get; set; }
    }
}
