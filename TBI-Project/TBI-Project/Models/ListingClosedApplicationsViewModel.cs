﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailAppManager.Models
{
    public class ListingClosedApplicationsViewModel
    {
        public List<ApplicationViewModel> ClosedApplications { get; set; }
    }
}
