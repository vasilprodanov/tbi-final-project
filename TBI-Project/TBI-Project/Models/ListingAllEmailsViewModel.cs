﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailAppManager.Models
{
    public class ListingAllEmailsViewModel
    {
        public List<EmailViewModel> AllEmails { get; set; }
    }
}
