﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TBI_Project.Data.Models;

namespace EmailAppManager.Models
{
    public class EmailViewModel
    {
        public int ID { get; set; }

        public string Sender { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public int AttachmentsCount { get; set; }

        public double AttachmentsSize { get; set; }

        public DateTime ReceivedOn { get; set; }

        public DateTime StoredOn { get; set; }

        public DateTime LastModifiedOn { get; set; }

        public string ProcessedBy { get; set; }

        public int StatusID { get; set; }

        public string Status { get; set; }

        public bool IsAcknowledged { get; set; }

        public string NextStatusName { get; set; }

        public string PrevStatusName { get; set; }

        public ApplicationViewModel Application { get; set; }
    }
}
