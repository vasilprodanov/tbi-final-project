﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailAppManager.Models
{
    public class ListingValidEmailsViewModel
    {
        public List<EmailViewModel> ValidEmails { get; set; }
    }
}
