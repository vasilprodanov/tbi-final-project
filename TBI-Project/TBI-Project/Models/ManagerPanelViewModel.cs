﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TBI_Project.Models.AccountViewModels;

namespace EmailAppManager.Models
{
    public class ManagerPanelViewModel
    {
        public RegisterViewModel RegisterModel { get; set; }

        public UserSearchViewModel SearchModel { get; set; }
    }
}
