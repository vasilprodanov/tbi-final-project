﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading;
using Microsoft.Extensions.DependencyInjection;
using TBI_Project.Data.Context;
using TBI_Project.Data.Models;
using Microsoft.AspNetCore.Identity;

namespace TBI_Project
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            //SeedData(host);

            host.Run();
        }

        private static async void SeedData(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<EmailAppManagerContext>();

                if (!context.Roles.Any(r => r.Name.ToLower() == "manager"))
                {
                    var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                    await roleManager.CreateAsync(new IdentityRole { Name = "Manager" });
                }


                if (!context.UserRoles.Any(ur => ur.RoleId == context.Roles.FirstOrDefault(r => r.Name.ToLower() == "manager").Id))
                {
                    var userManager = scope.ServiceProvider.GetRequiredService<UserManager<AppUser>>();
                    var managerUser1 = new AppUser { UserName = "vasilprodanov96@gmail.com", Email = "vasilprodanov96@gmail.com" };
                    var managerUser2 = new AppUser { UserName = "AppManager2", Email = "veselin.ignatov@protonmail.com" };

                    await userManager.CreateAsync(managerUser1, "Vasko1234@");
                    await userManager.CreateAsync(managerUser2, "Vesko1234@");

                    await userManager.AddToRoleAsync(managerUser1, "Manager");
                    await userManager.AddToRoleAsync(managerUser2, "Manager");
                }

                if (!context.EmailStatuses.Any())
                {
                    await context.EmailStatuses.AddAsync(new EmailStatus { StatusName = "Not Reviewed" });
                    await context.EmailStatuses.AddAsync(new EmailStatus { StatusName = "New" });
                    await context.EmailStatuses.AddAsync(new EmailStatus { StatusName = "Not Valid" });
                    await context.EmailStatuses.AddAsync(new EmailStatus { StatusName = "Processed" });
                }

                if (!context.ApplicationStatuses.Any())
                {
                    await context.ApplicationStatuses.AddAsync(new ApplicationStatus { StatusName = "Open" });
                    await context.ApplicationStatuses.AddAsync(new ApplicationStatus { StatusName = "Approved" });
                    await context.ApplicationStatuses.AddAsync(new ApplicationStatus { StatusName = "Rejected" });
                }
                context.SaveChanges();
            }
        }
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
            //.UseUrls("http")
                .Build();
    }
}
