﻿using EmailAppManager.Mappers;
using EmailAppManager.Services;
using EmailAppManager.Services.Contracts;
using EmailAppManager.Services.Services;
using EmailManager.Services;
using EmailManager.Services.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using TBI_Project.Data.Context;
using TBI_Project.Data.GoogleConfig;
using TBI_Project.Data.Models;
using TBI_Project.Services;

namespace TBI_Project
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<EmailAppManagerContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<IAppEmailService, AppEmailService>();

            services.AddScoped<IListingServices, ListingServices>();

            services.AddScoped<IApplicationServices, ApplicationServices>();

            services.AddScoped<IRegisterService, RegisterService>();

            services.AddScoped<IUserService, UserService>();

            services.AddScoped<IGoogleAuthService, GoogleAuthService>();

            services.AddScoped<IGmailApiService, GmailApiService>();

            services.Configure<GmailApiUrlOptions>(this.Configuration.GetSection("GmailApiUrlOptions"));
            services.Configure<GoogleAuthOptions>(this.Configuration.GetSection("GoogleAuthOptions"));

            Crypteron.CrypteronConfig.Config.MyCrypteronAccount.AppSecret
                = Configuration["CrypteronConfig:MyCrypteronAccount:AppSecret"];

            services.AddHttpClient();

            services.AddIdentity<AppUser, IdentityRole>()
                .AddEntityFrameworkStores<EmailAppManagerContext>()
                .AddDefaultTokenProviders();

            services.AddCustomMappers();
            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddHostedService<TimedHostedService>();

            services.AddMvc().AddJsonOptions
                (options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                //app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "localhost",
                    template: "{controller=Gmail}/{action=StartProgram}/{id?}");
            });
        }

        //private UserCredential GetGoogleCredentials()
        //{
        //   //string applicationName = "E-Mail applications manager";//"Gmail API .NET Quickstart";
        //    string[] scopes = { GmailService.Scope.GmailReadonly };

        //    using (var stream =
        //        new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
        //    {
        //        // The file token.json stores the user's access and refresh tokens, and is created
        //        // automatically when the authorization flow completes for the first time.
        //        string credPath = "token.json";
        //        return GoogleWebAuthorizationBroker.AuthorizeAsync(
        //            GoogleClientSecrets.Load(stream).Secrets,
        //            scopes,
        //            "user", 
        //            CancellationToken.None,
        //            new FileDataStore(credPath, true)).Result;
        //    }            
        //}
    }
}
