﻿using EmailAppManager.Mappers;
using EmailAppManager.Models;
using EmailAppManager.Services.Contracts;
using EmailManager.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TBI_Project.Data.Models;
using TBI_Project.Models;

namespace TBI_Project.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAppEmailService emailService;
        private readonly IGoogleAuthService gmailService;
        private readonly IGmailApiService apiService;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IViewModelMapper<Email, EmailViewModel> emailMapper;
        private readonly IViewModelMapper<IReadOnlyCollection<Email>, HomeViewModel> homeMapper;


        public HomeController(
            IAppEmailService emailService,
            IGoogleAuthService gmailService,
            IGmailApiService apiService,
            IHttpContextAccessor httpContextAccessor,
            IViewModelMapper<Email, EmailViewModel> emailMapper,
            IViewModelMapper<IReadOnlyCollection<Email>, HomeViewModel> homeMapper)
        {
            this.emailService = emailService ?? throw new ArgumentNullException(nameof(emailService));
            this.gmailService = gmailService ?? throw new ArgumentNullException(nameof(gmailService));
            this.apiService = apiService ?? throw new ArgumentNullException(nameof(apiService));
            this.httpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));
            this.emailMapper = emailMapper ?? throw new ArgumentNullException(nameof(emailMapper));
            this.homeMapper = homeMapper ?? throw new ArgumentNullException(nameof(homeMapper));
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {

            //var gmailAuthService = scope.ServiceProvider.GetRequiredService<IGoogleAuthService>();

            //var credentials = await gmailService.AcquireCredentialsAsync();

            //if (credentials == null)
            //{
            //    //return;
            //}
            //else
            //{
            //var accessToken = await gmailService.GetAccessToken(credentials);

            //var gmailApiService = scope.ServiceProvider.GetRequiredService<IGmailApiService>();

            //await apiService.SaveEmailsToDbAsync(accessToken);

            //this.logger.LogInformation("Scoped service id: " + service.Id);
            //}

            var credentials = await gmailService.CheckForTokensAsync();
            var accessToken = await gmailService.GetAccessToken(credentials);
            // this.emailService.SetInitialStatusAsync();
            // var emails = await this.apiService.SyncEmails(accessToken);
            //var latest = this.emailService.GetAllEmails(5);

            var homeViewModel = new HomeViewModel
            {
                LatestEmails = this.emailService
                                   .GetAllEmails(5)
                                   .Select(e => emailMapper.PartialMapFrom(e))
                                   .ToList(),

                LastModified = this.emailService
                                   .GetLastModifiedEmails(5)
                                   .Select(e => emailMapper.PartialMapFrom(e))
                                   .ToList(),

                ApprovedAndNew = this.emailService
                                     .GetApprovedAndNewEmails(5)
                                     .Select(e => emailMapper.PartialMapFrom(e))
                                     .ToList(),

            };
            return View(homeViewModel);
        }

        //public IActionResult ListAllEmails()
        //{
        //    return View();
        //}

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
