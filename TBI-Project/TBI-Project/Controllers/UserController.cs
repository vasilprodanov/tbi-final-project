﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmailAppManager.Mappers;
using EmailAppManager.Models;
using EmailAppManager.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TBI_Project.Data.Context;
using TBI_Project.Data.Models;
using TBI_Project.Models.AccountViewModels;

namespace EmailAppManager.Controllers
{
    
    public class UserController : Controller
    {
        private readonly IAppEmailService appEmailService;
        private readonly IUserService userService;
        private readonly IApplicationServices applicationService;
        private readonly IViewModelMapper<AppUser, UserViewModel> userMapper;
        private readonly IHttpContextAccessor contextAccessor;

        public UserController(
            IAppEmailService appEmailService,
            IUserService userService,
            IApplicationServices applicationService,
            IViewModelMapper<AppUser, UserViewModel> userMapper,
            IHttpContextAccessor contextAccessor)
        {
            this.appEmailService = appEmailService;
            this.userService = userService;
            this.applicationService = applicationService;
            this.userMapper = userMapper;
            this.contextAccessor = contextAccessor;
        }

        [Authorize(Roles = "Manager")]
        public IActionResult ManagerPanel()
        {
            return View(new ManagerPanelViewModel());
        }

        [Authorize(Roles = "Manager")]
        public IActionResult Registration([FromQuery]RegisterViewModel model)
        {
            return Redirect($"/account/register/");
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> UserSearch([FromQuery]UserSearchViewModel model)
        {
            var managerPanelVm = new ManagerPanelViewModel { SearchModel = model };

            if (string.IsNullOrWhiteSpace(model/*.SearchModel*/.SearchName) ||
                model/*.SearchModel*/.SearchName.Length < 3)
            {
                return View("ManagerPanel", managerPanelVm);
            }

            model/*.SearchModel*/.SearchResults = (await this.userService.SearchAsync(model/*.SearchModel*/.SearchName))
                                                    .Select(this.userMapper.MapFrom)
                                                    .ToList();
            
            
            return View("ManagerPanel", managerPanelVm);
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> RevertStatus(int id)
        {
            var userId = contextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var email = await this.appEmailService.RevertStatusAsync(id, userId);

            return Redirect("/home/index");
            //return Redirect($"/email/details/{id}");
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> RevertAppStatus(int id)
        {
            var userId = contextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var email = await this.applicationService.RevertStatusAsync(id, userId);

            return Redirect("/home/index");
            //return Redirect($"/email/details/{id}");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> InvalidStatus(int id)
        {
            var userId = contextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var email = await this.appEmailService.HandleRejectStatusesAsync(id, userId);

            return Redirect("/home/index");
           // return Redirect($"/email/details/{id}");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> SuccessStatus(int id)
        {
            var userId = contextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var email = await this.appEmailService.HandleSuccessStatusesAsync(id, userId);

            return Redirect("/home/index");
            //return Redirect($"/email/details/{id}");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> InvalidApplicationStatus(int id)
        {
            var userId = contextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var application = await this.applicationService.HandleRejectStatusesAsync(id, userId);

            return Redirect("/home/index");
            //return Redirect($"/application/details/{id}");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> SuccessApplicationStatus(int id)
        {
            var userId = contextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var application = await this.applicationService.HandleSuccessStatusesAsync(id, userId);

            return Redirect("/home/index");
            //return Redirect($"/application/details/{id}");
        }
    }
}