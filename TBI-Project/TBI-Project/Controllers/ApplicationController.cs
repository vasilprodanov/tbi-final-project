﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crypteron.CipherObject;
using EmailAppManager.Mappers;
using EmailAppManager.Models;
using EmailAppManager.Services.Contracts;
using EmailManager.Services.Contracts;
using Google.Apis.Gmail.v1;
using Microsoft.AspNetCore.Mvc;
using TBI_Project.Data.Context;
using TBI_Project.Data.Models;

namespace EmailAppManager.Controllers
{
    public class ApplicationController : Controller
    {
        private readonly EmailAppManagerContext context;
        private readonly IAppEmailService emailService;
        private readonly IGmailApiService apiService;
        private readonly IListingServices listingServices;
        private readonly IApplicationServices applicationServices;
        private readonly IViewModelMapper<Application, ApplicationViewModel> applicationMapper;
        private readonly IViewModelMapper<Email, EmailViewModel> emailMapper;
        private readonly IViewModelMapper<IReadOnlyCollection<Application>, ListingOpenApplicationsViewModel> openApplicationsListingMapper;
        private readonly IViewModelMapper<IReadOnlyCollection<Application>, ListingClosedApplicationsViewModel> closedApplicationsListingMapper;


        public ApplicationController(
            EmailAppManagerContext context,
            IAppEmailService emailService,
            IGmailApiService apiService,
            IListingServices listingServices,
            IApplicationServices applicationServices,
            IViewModelMapper<Application, ApplicationViewModel> applicationMapper,
            IViewModelMapper<Email, EmailViewModel> emailMapper,
            IViewModelMapper<IReadOnlyCollection<Email>, HomeViewModel> homeMapper,
            IViewModelMapper<IReadOnlyCollection<Application>, ListingOpenApplicationsViewModel> openApplicationsListingMapper,
            IViewModelMapper<IReadOnlyCollection<Application>, ListingClosedApplicationsViewModel> closedApplicationsListingMapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.emailService = emailService ?? throw new ArgumentNullException(nameof(emailService));
            this.apiService = apiService ?? throw new ArgumentNullException(nameof(apiService));
            this.listingServices = listingServices ?? throw new ArgumentNullException(nameof(listingServices));
            this.applicationServices = applicationServices ?? throw new ArgumentNullException(nameof(applicationServices));
            this.applicationMapper = applicationMapper ?? throw new ArgumentNullException(nameof(applicationMapper));
            this.emailMapper = emailMapper ?? throw new ArgumentNullException(nameof(emailMapper));
            this.openApplicationsListingMapper = openApplicationsListingMapper ?? throw new ArgumentNullException(nameof(openApplicationsListingMapper));
            this.closedApplicationsListingMapper = closedApplicationsListingMapper ?? throw new ArgumentNullException(nameof(closedApplicationsListingMapper));
        }

        public async Task<IActionResult> Details(int id)
        {
            var application = await applicationServices.GetApplication(id);
            //application.Unseal();

            if (application == null)
                return NotFound();

            return PartialView("_ApplicationDetailsPartial", this.applicationMapper.MapFrom(application));
        }


        public IActionResult ListOpenApplications()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> PageListOpenApplications()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            int totalRecord = this.applicationServices.GetOpenApplicationsCount();
            //if (totalRecord == 0)
            //{
            //    return Json(new { draw = draw, recordsFiltered = totalRecord, recordsTotal = totalRecord, data = new ApplicationViewModel() });
            //}
            var applications = await this.listingServices.GetAllOpenApplicationsAsync(skip, pageSize, searchValue);

            var model = applications.Select(a => this.applicationMapper.MapFrom(a)).ToList();

            return Json(new { draw = draw, recordsFiltered = totalRecord, recordsTotal = totalRecord, data = model });
        }

        public IActionResult ListClosedApplications()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> PageListClosedApplications()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            int totalRecord = this.applicationServices.GetClosedApplicationsCount();
            var applications = await this.listingServices.GetAllClosedApplicationsAsync(skip, pageSize, searchValue);

            var model = applications.Select(a => this.applicationMapper.MapFrom(a)).ToList();

            return Json(new { draw = draw, recordsFiltered = totalRecord, recordsTotal = totalRecord, data = model });
        }

        //Test action to check if the form works
        public IActionResult TestSuccess()
        {
            return View();
        }

        [HttpGet]
        public IActionResult CreateApplication()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateApplication(ApplicationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var applicationParameters = new List<string>
                {
                    model.CustomerFirstName,
                    model.CustomerSecondName,
                    model.PhoneNumber,
                    model.EmailOriginalID
                };

                var customerID = model.CustomerID;

                var loanAmount = model.LoanAmount;

                var period = model.PeriodInMonths;

                var application = this.applicationServices.CreateApplication(applicationParameters, customerID, loanAmount, period);

                return Redirect("/home/index");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet]
        public async Task<IActionResult> ApplicationDetailsModal(int id)
        {
            var application = await applicationServices.GetApplication(id);

            if (application == null)
                return NotFound();

            var model = new EmailDetailsModalModel();
            model.Application = this.applicationMapper.MapFrom(application);

            return PartialView("_EmailDetailsModal", model);
        }

        [HttpGet]
        public async Task<IActionResult> FooterDetails(int id)
        {
            var application = await applicationServices.GetApplication(id);

            if (application == null)
                return NotFound();

            var model = this.applicationMapper.MapFrom(application);

            return PartialView("_AppModalFooter", model);
        }
    }
}