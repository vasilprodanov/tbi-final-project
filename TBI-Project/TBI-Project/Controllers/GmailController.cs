﻿using EmailAppManager.Services.Contracts;
using EmailManager.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EmailManager.Web.Controllers
{
    public class GmailController : Controller
    {
        private readonly IGmailApiService gmailService;
        private readonly IGoogleAuthService gmailAuthService;

        public GmailController(IGmailApiService gmailService,
            IGoogleAuthService gmailAuthService)
        {
            this.gmailService = gmailService ?? throw new ArgumentNullException(nameof(gmailService));
            this.gmailAuthService = gmailAuthService ?? throw new ArgumentNullException(nameof(gmailAuthService));
        }

        public async Task<IActionResult> StartProgram()
        {
            var credentials = await gmailAuthService.CheckForTokensAsync();

            if (credentials == null)
            {
                return RedirectToAction("GoogleLogin", "Gmail");
            }
            else
            {
                var accessToken = await gmailAuthService.GetAccessToken(credentials);
                await gmailService.SyncEmails(accessToken);

                return RedirectToAction("Index", "Home");
            }
        }

        public IActionResult GoogleLogin()
        {
            #region Azure
            //var sb = new StringBuilder()
            //    .Append("https://accounts.google.com/o/oauth2/v2/auth?")
            //    .Append("scope=https://www.googleapis.com/auth/gmail.readonly")
            //    .Append("&access_type=offline")
            //    .Append("&include_granted_scopes=true")
            //    .Append("&response_type=code") + Static members

            // .Append("&redirect_uri=https://emailmanagertbi.azurewebsites.net/google-callback")
            // .Append("&client_id=622099583190-omjg8vkrkrhrcas4avbvkfjv72fsv4vq.apps.googleusercontent.com");
            #endregion
            var sb = gmailAuthService.BuildConsentUrl();
            return Redirect(sb.ToString());
        }
        [Route("google-callback")]
        public async Task<IActionResult> GoogleCallback(string code)
        {
            var accessToken = await gmailAuthService.AcquireCredentialsAsync(code);
            //var result = await gmailService.ListMessages(accessToken);
            await gmailService.SyncEmails(accessToken);
            return RedirectToAction("Index", "Home");
            #region Azure and old Hardcalled local request
            //var client = new HttpClient();
            //var res = await client.PostAsync("https://oauth2.googleapis.com/token", content);
            //    var content = new FormUrlEncodedContent(new[]
            //    {
            //                    #region Azure
            //                    //new KeyValuePair<string,string>("code",code),
            //                    //new KeyValuePair<string,string>("client_id","622099583190-omjg8vkrkrhrcas4avbvkfjv72fsv4vq.apps.googleusercontent.com"),
            //                    //new KeyValuePair<string,string>("client_secret","b8r0hoqa2fT4qwpeXtS_Abtg"),
            //                    //new KeyValuePair<string,string>("redirect_uri","https://emailmanagertbi.azurewebsites.net/google-callback"),
            //                    //new KeyValuePair<string,string>("grant_type","authorization_code"),
            //                    new KeyValuePair<string,string>("code",code),
            //                    new KeyValuePair<string,string>("client_id","752865131593-15dgn2cou2uhjdshtsvf4b7gaq6qbt9m.apps.googleusercontent.com"),
            //                    new KeyValuePair<string,string>("client_secret","NpZHmyqD_9835TMfu5Hvn1tt"),
            //                    new KeyValuePair<string,string>("redirect_uri","https://localhost:5001/google-callback"),
            //                    new KeyValuePair<string,string>("grant_type","authorization_code"),
            //                });
            //    if (!res.IsSuccessStatusCode)
            //    {
            //return Json(await res.Content.ReadAsStringAsync());
            //    }
            //else
            //{
            //    gmailAuthService.SaveCredentials(await res.Content.ReadAsStringAsync());

            //    var userData = JsonConvert.DeserializeObject<UserData>(await res.Content.ReadAsStringAsync());
            //    return Json(await TryReadGmail(client, userData));
            #endregion
        }
    }
}
