﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using EmailAppManager.Mappers;
using EmailAppManager.Models;
using EmailAppManager.Services.Contracts;
using EmailManager.Services.Contracts;
using Google.Apis.Gmail.v1;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TBI_Project.Data.Context;
using TBI_Project.Data.Models;

namespace EmailAppManager.Controllers
{
    public class EmailController : Controller
    {
        private readonly EmailAppManagerContext context;
        private readonly IAppEmailService emailService;
        private readonly IGoogleAuthService googleService;
        private readonly IGmailApiService apiService;
        private readonly IListingServices listingServices;
        private readonly IViewModelMapper<Email, EmailViewModel> emailMapper;
        private readonly IViewModelMapper<IReadOnlyCollection<Email>, ListingAllEmailsViewModel> allEmailsListingMapper;
        private readonly IViewModelMapper<IReadOnlyCollection<Email>, ListingValidEmailsViewModel> validEmailsListingMapper;


        public EmailController(
            EmailAppManagerContext context,
            IAppEmailService emailService,
            IGoogleAuthService googleService,
            IGmailApiService apiService,
            IListingServices listingServices,
            IViewModelMapper<Email, EmailViewModel> emailMapper,
            IViewModelMapper<IReadOnlyCollection<Email>, HomeViewModel> homeMapper,
            IViewModelMapper<IReadOnlyCollection<Email>, ListingAllEmailsViewModel> allEmailsListingMapper,
            IViewModelMapper<IReadOnlyCollection<Email>, ListingValidEmailsViewModel> validEmailsListingMapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.emailService = emailService ?? throw new ArgumentNullException(nameof(emailService));
            this.googleService = googleService ?? throw new ArgumentNullException(nameof(googleService));
            this.apiService = apiService ?? throw new ArgumentNullException(nameof(apiService));
            this.listingServices = listingServices ?? throw new ArgumentNullException(nameof(listingServices));
            this.emailMapper = emailMapper ?? throw new ArgumentNullException(nameof(emailMapper));
            this.allEmailsListingMapper = allEmailsListingMapper ?? throw new ArgumentNullException(nameof(allEmailsListingMapper));
            this.validEmailsListingMapper = validEmailsListingMapper ?? throw new ArgumentNullException(nameof(validEmailsListingMapper));
        }

        [Authorize]
        public async Task<IActionResult> Details(int id)
        {
            var credentials = await this.googleService.CheckForTokensAsync();
            var accessToken = await this.googleService.GetAccessToken(credentials);

            var email = await this.apiService.FetchMessageBodyAsync(id, accessToken);

            if (email == null)
                return NotFound();

            var body = email.Body;
            return Content(body);
        }

        [Authorize]
        public async Task<IActionResult> BodyContent(int id)
        {
            var credentials = await this.googleService.CheckForTokensAsync();
            var accessToken = await this.googleService.GetAccessToken(credentials);

            var email = await this.apiService.GetMessageBodyAsync(id, accessToken);

            if (email == null)
                return NotFound();

            var body = email.Body;

            return Content(body);
        }

        public async Task<IActionResult> ApplicationForm(int id)
        {
            var email = await this.context.Emails.FindAsync(id);

            if (email == null)
                return NotFound();

            return PartialView("_CreateApplicationPartial", new ApplicationViewModel());
        }

        public IActionResult ListAllEmails()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> PageList()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            int totalRecord = this.emailService.GetEmailsCount();
            var emails = await this.emailService.ListAllEmailsAsync(skip, pageSize, searchValue);

           var model = emails.Select(e => this.emailMapper.MapFrom(e)).ToList();

            return Json(new { draw = draw, recordsFiltered = totalRecord, recordsTotal = totalRecord, data = model });
        }

        public IActionResult ListValidEmails()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> PageListValidEmails()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            int totalRecord = this.listingServices.GetValidEmailsCount();
            var emails = await this.listingServices.GetAllValidEmailsAsync(skip, pageSize, searchValue);

            var model = emails.Select(x => this.emailMapper.MapFrom(x)).ToList();

            return Json(new { draw = draw, recordsFiltered = totalRecord, recordsTotal = totalRecord, data = model });

        }

        //public IActionResult ModalBody(int id)
        //{
        //    var email = this.emailMapper.MapFrom(this.context.Emails.Find(id));
        //    email.Body = this.Details()

        //    return PartialView("_ModalFooter", email);
        //}

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ProvideFooter(int id)
        {
            var email = await this.context.Emails.FindAsync(id);

            var model = this.emailMapper.MapFrom(email);

            return PartialView("_ModalFooter", model);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> EmailDetailsModal(int id)
        {
            var credentials = await this.googleService.CheckForTokensAsync();
            var accessToken = await this.googleService.GetAccessToken(credentials);
            var email = await this.context.Emails.FindAsync(id);

            if (email.StatusID == 1)
            {
                email = await this.apiService.FetchMessageBodyAsync(id, accessToken);
            }
            else
            {
                email = await this.apiService.GetMessageBodyAsync(id, accessToken);
            }

            if (email == null)
                return NotFound();

            var model = new EmailDetailsModalModel();
            model.Email = this.emailMapper.MapFrom(email);
            model.Application = new ApplicationViewModel();

            return PartialView("_EmailDetailsModal", model);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> EmailNotReviewedModal(int id)
        {
            var credentials = await this.googleService.CheckForTokensAsync();
            var accessToken = await this.googleService.GetAccessToken(credentials);

            var email = await this.apiService.GetMessageBodyAsync(id, accessToken);

            if (email == null)
                return NotFound();

            var model = new EmailDetailsModalModel();
            model.Email = this.emailMapper.MapFrom(email);
            model.Application = new ApplicationViewModel();

            return PartialView("_EmailDetailsModal", model);
        }

        [Authorize]
        public IActionResult ListLastModified()
        {
            IReadOnlyCollection<Email> emails = this.emailService.GetLastModifiedEmails();

            ListingAllEmailsViewModel vm = this.allEmailsListingMapper.MapFrom(emails);

            return View(vm);
        }
    }
}