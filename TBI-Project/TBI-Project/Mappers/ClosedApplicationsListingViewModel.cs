﻿using Crypteron.CipherObject;
using EmailAppManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TBI_Project.Data.Models;

namespace EmailAppManager.Mappers
{
    public class ClosedApplicationsListingViewModelMapper : IViewModelMapper<IReadOnlyCollection<Application>, ListingClosedApplicationsViewModel>
    {
        private readonly IViewModelMapper<Application, ApplicationViewModel> applicationsMapper;

        public ClosedApplicationsListingViewModelMapper(IViewModelMapper<Application, ApplicationViewModel> applicationsMapper)
        {
            this.applicationsMapper = applicationsMapper ?? throw new ArgumentNullException(nameof(applicationsMapper));
        }

        public ListingClosedApplicationsViewModel MapFrom(IReadOnlyCollection<Application> entity)
         => new ListingClosedApplicationsViewModel
         {

             ClosedApplications = entity.Select(this.applicationsMapper.MapFrom).ToList()         };

        //Useless method, implemented just to comply with the interface
        public ListingClosedApplicationsViewModel PartialMapFrom(IReadOnlyCollection<Application> entity)
         => new ListingClosedApplicationsViewModel
         {
             ClosedApplications = entity.Select(this.applicationsMapper.PartialMapFrom).ToList()
         };
    }
}
