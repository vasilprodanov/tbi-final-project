﻿using EmailAppManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TBI_Project.Data.Models;

namespace EmailAppManager.Mappers
{
    public class UserViewModelMapper : IViewModelMapper<AppUser, UserViewModel>
    {
        public UserViewModel MapFrom(AppUser entity)
             => new UserViewModel
             {
                 Id = entity.Id,
                 UserName = entity.UserName,
                 Email = entity.Email,
                 RegisteredBy = entity.RegisteredBy,
                 RegisteredOn = entity.RegisteredOn
             };

        public UserViewModel PartialMapFrom(AppUser entity)
        {
            throw new NotImplementedException();
        }
    }
}