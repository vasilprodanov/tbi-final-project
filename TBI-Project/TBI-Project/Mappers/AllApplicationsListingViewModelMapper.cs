﻿using Crypteron.CipherObject;
using EmailAppManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TBI_Project.Data.Models;

namespace EmailAppManager.Mappers
{
    public class AllApplicationsListingViewModelMapper : IViewModelMapper<IReadOnlyCollection<Application>, ListingOpenApplicationsViewModel>
    {
        private readonly IViewModelMapper<Application, ApplicationViewModel> applicationsMapper;

        public AllApplicationsListingViewModelMapper(IViewModelMapper<Application, ApplicationViewModel> applicationsMapper)
        {
            this.applicationsMapper = applicationsMapper ?? throw new ArgumentNullException(nameof(applicationsMapper));
        }

        public ListingOpenApplicationsViewModel MapFrom(IReadOnlyCollection<Application> entity)
         => new ListingOpenApplicationsViewModel
         {
             OpenApplications = entity/*.Select(e => e.Unseal())*/.Select(this.applicationsMapper.MapFrom).ToList()
             
         };

        //Useless method, implemented just to comply with the interface
        public ListingOpenApplicationsViewModel PartialMapFrom(IReadOnlyCollection<Application> entity)
         => new ListingOpenApplicationsViewModel
         {
             OpenApplications = entity.Select(this.applicationsMapper.PartialMapFrom).ToList()
         };
    }
}
