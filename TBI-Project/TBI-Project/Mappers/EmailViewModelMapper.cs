﻿using EmailAppManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TBI_Project.Data.Context;
using TBI_Project.Data.Models;

namespace EmailAppManager.Mappers
{
    public class EmailViewModelMapper : IViewModelMapper<Email, EmailViewModel>
    {
        private readonly EmailAppManagerContext context;

        public EmailViewModelMapper(EmailAppManagerContext context)
        {
            this.context = context;
        }

        public EmailViewModel MapFrom(Email entity)
         => new EmailViewModel
         {
             ID = entity.ID,
             Subject = entity.Subject,
             Sender = entity.Sender,
             StatusID = entity.StatusID,
             Body = entity.Body,
             Status = this.context.EmailStatuses.FirstOrDefault(s => s.ID == entity.StatusID).StatusName,
             ProcessedBy = entity.ProcessedBy?.UserName,
             StoredOn = entity.StoredOn,
             AttachmentsCount = entity.AttachmentsCount,
             ReceivedOn = entity.ReceivedOn,
             IsAcknowledged = entity.IsAcknowledged,
             LastModifiedOn = entity.LastModifiedOn,
         };

        public EmailViewModel PartialMapFrom(Email entity)
         => new EmailViewModel
         {
             ID = entity.ID,
             Sender = entity.Sender,
             Subject = entity.Subject,
             StoredOn = entity.StoredOn,
             StatusID = entity.StatusID,
             Status = this.context.EmailStatuses.FirstOrDefault(s => s.ID == entity.StatusID).StatusName, 
             LastModifiedOn = entity.LastModifiedOn,
             IsAcknowledged = entity.IsAcknowledged,
             Body = entity.Body,
         };
    }
}
