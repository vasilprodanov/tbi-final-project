﻿using EmailAppManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TBI_Project.Data.Models;

namespace EmailAppManager.Mappers
{
    public class ValidEmailsListingViewModelMapper : IViewModelMapper<IReadOnlyCollection<Email>, ListingValidEmailsViewModel>
    {
        private readonly IViewModelMapper<Email, EmailViewModel> emailsMapper;

        public ValidEmailsListingViewModelMapper(IViewModelMapper<Email, EmailViewModel> emailsMapper)
        {
            this.emailsMapper = emailsMapper ?? throw new ArgumentNullException(nameof(emailsMapper));
        }

        public ListingValidEmailsViewModel MapFrom(IReadOnlyCollection<Email> entity)
         => new ListingValidEmailsViewModel
         {
             ValidEmails = entity.Select(this.emailsMapper.PartialMapFrom).ToList()
         };

        //Useless method, implemented just to comply with the interface
        public ListingValidEmailsViewModel PartialMapFrom(IReadOnlyCollection<Email> entity)
         => new ListingValidEmailsViewModel
         {
             ValidEmails = entity.Select(this.emailsMapper.PartialMapFrom).ToList()
         };
    }
}
