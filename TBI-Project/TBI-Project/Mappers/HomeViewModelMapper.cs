﻿using EmailAppManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TBI_Project.Data.Models;

namespace EmailAppManager.Mappers
{
    public class HomeViewModelMapper : IViewModelMapper<IReadOnlyCollection<Email>, HomeViewModel>
    {
        private readonly IViewModelMapper<Email, EmailViewModel> emailMapper;

        public HomeViewModelMapper(IViewModelMapper<Email, EmailViewModel> emailMapper)
        {
            this.emailMapper = emailMapper ?? throw new ArgumentNullException(nameof(emailMapper));
        }

        public HomeViewModel MapFrom(IReadOnlyCollection<Email> entity)
        => new HomeViewModel
        {
            LatestEmails = entity.Select(this.emailMapper.MapFrom).ToList(),
            LastModified = entity.Select(this.emailMapper.MapFrom).ToList(),
            ApprovedAndNew = entity.Select(this.emailMapper.MapFrom).ToList(),
        };

        public HomeViewModel PartialMapFrom(IReadOnlyCollection<Email> entity)

        {
            return new HomeViewModel
            {
                LatestEmails = entity.Select(this.emailMapper.PartialMapFrom).ToList(),
                LastModified = entity.Select(this.emailMapper.MapFrom).ToList(),
                ApprovedAndNew = entity.Select(this.emailMapper.MapFrom).ToList(),
            };
        }
    }
}
