﻿using EmailAppManager.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TBI_Project.Data.Models;

namespace EmailAppManager.Mappers
{
    public static class MappersRegistrator
    {
        public static IServiceCollection AddCustomMappers(this IServiceCollection services)
        {
            services.AddScoped<IViewModelMapper<Email, EmailViewModel>, EmailViewModelMapper>();
            services.AddScoped<IViewModelMapper<IReadOnlyCollection<Email>, HomeViewModel>, HomeViewModelMapper>();
            services.AddScoped<IViewModelMapper<IReadOnlyCollection<Email>, ListingAllEmailsViewModel>, AllEmailsListingViewModelMapper >();
            services.AddScoped<IViewModelMapper<IReadOnlyCollection<Email>, ListingValidEmailsViewModel>, ValidEmailsListingViewModelMapper>();
            services.AddScoped<IViewModelMapper<Application, ApplicationViewModel>, ApplicationViewModelMapper> ();
            services.AddScoped<IViewModelMapper<IReadOnlyCollection<Application>, ListingOpenApplicationsViewModel>, AllApplicationsListingViewModelMapper>();
            services.AddScoped<IViewModelMapper<IReadOnlyCollection<Application>, ListingClosedApplicationsViewModel>, ClosedApplicationsListingViewModelMapper>();
            services.AddScoped<IViewModelMapper<AppUser, UserViewModel>, UserViewModelMapper>();
            return services;
        }
    }
}
