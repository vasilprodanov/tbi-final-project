﻿using EmailAppManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TBI_Project.Data.Context;
using TBI_Project.Data.Models;

namespace EmailAppManager.Mappers
{
    public class ApplicationViewModelMapper : IViewModelMapper<Application, ApplicationViewModel>
    {
        private readonly EmailAppManagerContext context;

        public ApplicationViewModelMapper(EmailAppManagerContext context)
        {
            this.context = context;
        }

        public ApplicationViewModel MapFrom(Application entity)
         => new ApplicationViewModel
         {
             ID = entity.ID,
             CustomerFirstName = entity.CustomerFirstName,
             CustomerSecondName = entity.CustomerSecondName,
             CustomerFullName = entity.CustomerFullName,
             CustomerID = entity.CustomerID,
             PhoneNumber = entity.PhoneNumber,
             LoanAmount = entity.LoanAmount,
             LoanPeriodInMonths = entity.LoanPeriodInMonths,

             RegisteredOn = entity.RegisteredOn,
             LastChangedOn = entity.LastChangedOn,
             TerminalStatusSetOn = entity.TerminalStatusSetOn,
             EmailOriginalID = entity.EmailOriginalID,
             IsDeleted = entity.IsDeleted,
             ApplicationStatusID = entity.ApplicationStatusID,
             StatusName = this.context.ApplicationStatuses.Find(entity.ApplicationStatusID).StatusName,
         };


        public ApplicationViewModel PartialMapFrom(Application entity)
         => new ApplicationViewModel
         {
             ID = entity.ID,
             RegisteredOn = entity.RegisteredOn
         };
    }
}
