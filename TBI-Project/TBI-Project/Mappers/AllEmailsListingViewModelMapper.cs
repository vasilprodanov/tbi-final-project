﻿using EmailAppManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TBI_Project.Data.Models;

namespace EmailAppManager.Mappers
{
    public class AllEmailsListingViewModelMapper : IViewModelMapper<IReadOnlyCollection<Email>, ListingAllEmailsViewModel>
    {
        private readonly IViewModelMapper<Email, EmailViewModel> emailsMapper;

        public AllEmailsListingViewModelMapper(IViewModelMapper<Email, EmailViewModel> emailsMapper)
        {
            this.emailsMapper = emailsMapper ?? throw new ArgumentNullException(nameof(emailsMapper));
        }

        public ListingAllEmailsViewModel MapFrom(IReadOnlyCollection<Email> entity)
         => new ListingAllEmailsViewModel
         {
             AllEmails = entity.Select(this.emailsMapper.PartialMapFrom).ToList()
         };

        //Useless method, implemented just to comply with the interface
        public ListingAllEmailsViewModel PartialMapFrom(IReadOnlyCollection<Email> entity)
         => new ListingAllEmailsViewModel
         {
             AllEmails = entity.Select(this.emailsMapper.PartialMapFrom).ToList()
         };
    }
}
