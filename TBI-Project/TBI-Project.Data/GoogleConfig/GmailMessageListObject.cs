﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TBI_Project.Data.GoogleConfig
{
    public class GmailMessageListObject
    {
        [JsonProperty("messages")]
        public GmailMessageObject[] Messages { get; set; }

        [JsonProperty("resultSizeEstimate")]
        public int SizeEstimate { get; set; }

        [JsonProperty("nextPageToken")]
        public string NextPageToken { get; set; }
    }
}
