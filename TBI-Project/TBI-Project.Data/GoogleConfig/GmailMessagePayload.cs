﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TBI_Project.Data.GoogleConfig
{
    public class GmailMessagePayload
    {
        [JsonProperty("headers")]
        public IList<MessagePartHeader> Headers { get; set; }

        [JsonProperty("parts")]
        public IList<MessagePart> Parts { get; set; }

        [JsonProperty("body")]
        public MessagePartBody Body { get; set; }
    }
}
