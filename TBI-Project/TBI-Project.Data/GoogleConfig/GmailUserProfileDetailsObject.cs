﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TBI_Project.Data.GoogleConfig
{
    public class GmailUserProfileDetailsObject
    {
        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("messagesTotal")]
        public int TotalMessagesCount { get; set; }

        [JsonProperty("threadsTotal")]
        public int TotalThreadsCount { get; set; }

        [JsonProperty("historyId")]
        public ulong History { get; set; }
    }
}
