﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TBI_Project.Data.GoogleConfig
{
    public class MessagePart
    {
        [JsonProperty("parts")]
        public IList<MessagePart> Parts { get; set; }

        [JsonProperty("body")]
        public MessagePartBody Body { get; set; }

        [JsonProperty("fileName")]
        public string FileName { get; set; }
    }
}
