﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TBI_Project.Data.GoogleConfig
{
    public class GmailMessageObject
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("threadId")]
        public string ThreadId { get; set; }

        [JsonProperty("internalDate")]
        public long InternalDate { get; set; }

        [JsonProperty("payload")]
        public GmailMessagePayload Payload { get; set; }
    }
}
