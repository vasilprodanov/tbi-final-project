﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TBI_Project.Data.GoogleConfig
{
    public class MessagePartHeader
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
