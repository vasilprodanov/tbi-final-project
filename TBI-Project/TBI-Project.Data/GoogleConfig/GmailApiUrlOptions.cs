﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TBI_Project.Data.GoogleConfig
{
    public class GmailApiUrlOptions
    {
        public string Base { get; set; }

        public string Messages { get; set; }

        public string Message { get; set; }

        public string UserProfile { get; set; }

        public string UserProfileDetails { get; set; }
    }
}
