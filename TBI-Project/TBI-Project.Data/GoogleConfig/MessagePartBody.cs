﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TBI_Project.Data.GoogleConfig
{
    public class MessagePartBody
    {
        [JsonProperty("data")]
        public string Data { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }
    }
}
