﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TBI_Project.Data.GoogleConfig
{
    public class GmailMessageLabelModifier
    {
        [JsonProperty("addLabelIds")]
        public List<string> LabelsToAdd { get; set; }

        [JsonProperty("removeLabelIds")]
        public List<string> LabelsToRemove { get; set; }
    }
}
