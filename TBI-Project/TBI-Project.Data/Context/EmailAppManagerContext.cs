﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TBI_Project.Data.Models;

namespace TBI_Project.Data.Context
{
    public class EmailAppManagerContext : IdentityDbContext<AppUser>
    {
        public EmailAppManagerContext(DbContextOptions options)
           : base(options)
        {

        }

        public DbSet<Application> Applications { get; set; }

        public DbSet<AppUser> AppUsers { get; set; }

        public DbSet<AuditLog> AuditLogs { get; set; }

        public DbSet<Email> Emails { get; set; }

        public DbSet<EmailStatus> EmailStatuses { get; set; }

        public DbSet<ApplicationStatus> ApplicationStatuses { get; set; }

        public DbSet<GoogleCredentials> GoogleCredentials { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
        }
    }
}
