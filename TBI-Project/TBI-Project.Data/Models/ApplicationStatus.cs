﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TBI_Project.Data.Models
{
    public class ApplicationStatus
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Status Name is required")]
        public string StatusName { get; set; }

        public bool IsDeleted { get; set; }

        ICollection<Email> Emails { get; set; }
    }
}
