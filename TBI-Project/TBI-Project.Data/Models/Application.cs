﻿using Crypteron;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TBI_Project.Data.Models
{
    public class Application
    {
        public int ID { get; set; }

        [Required]
        public DateTime RegisteredOn { get; set; }

        [Required]
        public DateTime LastChangedOn { get; set; }

        //[Required]
        public DateTime TerminalStatusSetOn { get; set; }

        public string EmailOriginalID { get; set; }

        public bool IsDeleted { get; set; }

        [Required]
        [Secure]
        public string CustomerID { get; set; }

        [Required]
        [Secure]
        public string CustomerFirstName { get; set; }

        [Required]
        [Secure]
        public string CustomerSecondName { get; set; }

        [Required]
        [Secure]
        public string CustomerFullName { get; set; }

        [Required]
        [Secure]
        public string PhoneNumber { get; set; }

        [Required]
        public decimal LoanAmount { get; set; }

        [Required]
        public int LoanPeriodInMonths { get; set; }

        // Navigational properties


        public int ApplicationStatusID { get; set; }
        public ApplicationStatus Status { get; set; }

        public string ProcessedById { get; set; }
        public AppUser ProcessedBy { get; set; }
    }
}
