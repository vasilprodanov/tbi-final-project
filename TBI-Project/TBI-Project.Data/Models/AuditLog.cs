﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TBI_Project.Data.Models
{
    public class AuditLog
    {
        public int ID { get; set; }

        public DateTime TimeStamp { get; set; }

        public string ActionPerformed { get; set; }

        public AppUser PerformedBy { get; set; }

        public string LastStatus { get; set; }

        public string NewStatus { get; set; }

        public bool IsDeleted { get; set; }
    }
}
