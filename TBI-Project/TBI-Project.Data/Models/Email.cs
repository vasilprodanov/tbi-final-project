﻿using Crypteron;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TBI_Project.Data.Models
{
    public class Email
    {
        public int ID { get; set; }

        [Required]
        public string OriginalId { get; set; }

        [Secure]
        public string Sender { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public int AttachmentsCount { get; set; }

        public double AttachmentsSize { get; set; }

        public DateTime ReceivedOn { get; set; }

        public DateTime StoredOn { get; set; }

        public DateTime LastModifiedOn { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsAcknowledged { get; set; }

        public string PrevStatusName { get; set; }

        public string NextStatusName { get; set; }

        // Navigational properties
        public int? ApplicationID { get; set; }
        public Application Application { get; set; }

        public int StatusID {get; set;}
        public EmailStatus Status { get; set; }

        public string ProcessedById { get; set; }
        public AppUser ProcessedBy { get; set; }
    }
}
