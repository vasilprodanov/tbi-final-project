﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace TBI_Project.Data.Models
{
    public class AppUser : IdentityUser
    {
        public AppUser() { }

        public DateTime RegisteredOn { get; set; }

        public string RegisteredBy { get; set; }

        public bool IsDeleted { get; set; }
    }
}
