﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TBI_Project.Data.Models
{
    public class GoogleCredentials
    {
        public int Id { get; set; }

        [MaxLength(512)]
        public string AccessToken { get; set; }

        [MaxLength(512)]
        public string RefreshToken { get; set; }

        public DateTime ExpirationTime { get; set; }
    }
}
